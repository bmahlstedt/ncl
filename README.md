Get the framework
```
pip install flask
```

Start the service
```
python myapp.py
```

Visit in browser
```
http://localhost:5000/
```

AWS will have to be reconfigured to run the app, rather than pointing to a single static file.

Next steps would be to implement a proper database for trending, backups, history visualization, etc.

Feel free to iterate / ask questions!
