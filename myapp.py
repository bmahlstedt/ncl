import os
import json
import random
from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)
datafile = 'mydata.json'


@app.route('/')
def home():
    # This would be your main view. The typical pattern here would be to read
    # data from a database. For now, you can simply read from a file.
    if os.path.isfile(datafile):
        with open(datafile) as fh:
            data = json.load(fh)
    else:
        data = {'temp': 0}
    return render_template('mytemplate.html', temp=data['temp'])


@app.route('/update-data', methods=['POST'])
def update_data():
    # This is the endpoint your radio could post to. It depends on how it's
    # formatted, but a common pattern is that request.form is simply a
    # dictionary of the data.
    #     example: temp = request.form['temp']
    # For now, it just generates a random number.
    temp = random.randint(1, 100)
    data = {'temp': temp}

    # Once you've extracted the data you want into python mem, you'd usually
    # write it to a database. For now, you can simply write it to a file.
    with open(datafile, 'w') as fh:
        json.dump(data, fh)

    # This function demonstrates how you receive/handle the post from the
    # radio, but your active buttons (heater control, force ascent, etc)
    # will act in a similar manner. The template has a form submit that
    # simply posts to an endpoint. You can pass data with that post, input
    # user data, or simply flip a switch (which seems the case for yours).
    # Your radio would then handle it in a similar manner as this function
    # does.

    return redirect(url_for('home'))


if __name__ == '__main__':
    app.run(debug=True)
